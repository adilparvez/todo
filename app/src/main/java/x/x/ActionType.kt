package x.x

enum class ActionType {
    ADD_TASK,
    REMOVE_TASK,
    TOGGLE_TASK,
    REMOVE_SELECTED_TASKS,
}