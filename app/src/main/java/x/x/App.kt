package x.x

import android.app.Application
import trikita.jedux.Action
import trikita.jedux.Store
import x.x.state.State

class App : Application() {

    companion object {
        lateinit var instance: Application
        lateinit var store: Store<Action<ActionType, *>, State>
        fun dispatch(a: Action<ActionType, *>) = store.dispatch(a)
        fun state() = store.state
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        store = Store(Reducer(), State.default())
    }

}