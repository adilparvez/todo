package x.x

import android.content.Context
import android.view.Gravity
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.Toast
import trikita.anvil.DSL.*
import trikita.anvil.RenderableView
import trikita.jedux.Action

class MainView(ctx : Context) : RenderableView(ctx) {

    var input = ""
    var taskAdapter = TaskAdapter()

    override fun view() {
        taskAdapter.notifyDataSetChanged()
        linearLayout {
            orientation(LinearLayout.VERTICAL)

            editText {
                imeOptions(EditorInfo.IME_ACTION_DONE)
                singleLine(true)
                text(input)
                onTextChanged { input = it.toString().trim() }
                onEditorAction {
                    view, action, event ->
                        if (action == EditorInfo.IME_ACTION_DONE && input.trim().length > 0) {
                            App.dispatch(Action<ActionType, String>(ActionType.ADD_TASK, input))
                            input = ""
                        }
                        false
                }
            }

            button {
                text("Clear")
                onClick { input = "" }
            }

            listView {
                size(FILL, WRAP)
                adapter(taskAdapter)
            }

            button {
                text("remove selected")
                layoutGravity(Gravity.BOTTOM)
                visibility(App.state().hasSelectedTasks())
                onClick { App.dispatch(Action<ActionType, Unit>(ActionType.REMOVE_SELECTED_TASKS)) }
            }
        }
    }

}
