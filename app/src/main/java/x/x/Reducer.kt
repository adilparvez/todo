package x.x

import android.util.Log
import com.github.andrewoma.dexx.collection.LinkedList
import com.github.andrewoma.dexx.collection.Set
import com.github.andrewoma.dexx.kollection.toImmutableList
import com.github.andrewoma.dexx.kollection.toImmutableSet
import trikita.jedux.Store
import trikita.jedux.Action
import x.x.ActionType.*
import x.x.state.*

class Reducer : Store.Reducer<Action<ActionType, *>, State> {

    override fun reduce(a: Action<ActionType, *>, s: State): State {
        val (tasks, nextId) = s
        when (a.type) {
            ADD_TASK -> {
                val name = a.value as String
                val newTask = Task(nextId, name, false)
                return State(tasks + newTask, nextId + 1)
            }
            REMOVE_TASK -> {
                val id = a.value as Int
                val newTasks = tasks.asSequence().filter {
                    !it.id.equals(id)
                }.toImmutableList()
                return State(newTasks, nextId)
            }

            TOGGLE_TASK -> {
                val (id, selected) = a.value as Pair<Int, Boolean>
                val newTasks = tasks.asSequence().map {
                    if (it.id.equals(id)) {
                        it.copy(selected = selected)
                    } else {
                        it
                    }
                }.toImmutableList()
                return State(newTasks, nextId)
            }
            REMOVE_SELECTED_TASKS -> {
                val newTasks = tasks.asSequence().filter {
                    !it.selected
                }.toImmutableList()
                return State(newTasks, nextId)
            }
            else -> { return s }
        }
    }

}
