package x.x

import android.view.Gravity
import android.widget.CompoundButton
import android.widget.LinearLayout
import trikita.anvil.DSL.*
import trikita.anvil.RenderableAdapter
import trikita.jedux.Action

class TaskAdapter : RenderableAdapter() {

    override fun view(idx: Int) {
        val task = getItem(idx)
        linearLayout {
            size(FILL, dip(50))
            orientation(LinearLayout.HORIZONTAL)
            gravity(Gravity.CENTER_VERTICAL)
            checkBox {
                checked(task.selected)
                text("[${idx}] ${task.name}")
                onCheckedChange {
                    btn: CompoundButton, isChecked: Boolean ->
                        App.dispatch(Action<ActionType, Pair<Int, Boolean>>(ActionType.TOGGLE_TASK, task.id to isChecked))
                }
            }
            button {
                onClick {
                    App.dispatch(Action<ActionType, Int>(ActionType.REMOVE_TASK, task.id))
                }
            }
        }
    }

    override fun getItem(idx: Int) = App.state().tasks.get(idx)

    override fun getCount() = App.state().tasks.size

}