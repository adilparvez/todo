package x.x.state

import com.github.andrewoma.dexx.kollection.ImmutableList
import com.github.andrewoma.dexx.kollection.immutableListOf

data class State(val tasks: ImmutableList<Task>, val nextId: Int) {

    fun hasSelectedTasks() = tasks.any { it.selected }

    companion object {
        fun default() = State(immutableListOf(), 0)
    }

}
