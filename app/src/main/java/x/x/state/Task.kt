package x.x.state

data class Task(val id: Int,
                val name: String,
                var selected: Boolean)
